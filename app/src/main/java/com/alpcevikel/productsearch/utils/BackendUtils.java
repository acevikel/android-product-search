package com.alpcevikel.productsearch.utils;

import android.net.Uri;

import com.alpcevikel.productsearch.classes.FormData;

public class BackendUtils {
    private static final String BACKEND_URL = "acevikelhw9.us-east-1.elasticbeanstalk.com";
    private static final String[] categories = {"all","550","2984","267","11450","58058","26395","11233","1249"};
    //todo handle category, selected option, from
    public static String getAdvancedSearchUrl(FormData formData) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority(BACKEND_URL)
                .appendPath("api")
                .appendPath("search")
                .appendQueryParameter("keyword",formData.keyword)
                .appendQueryParameter("category", categories[formData.category])
                .appendQueryParameter("conditionNew",String.valueOf(formData.conditionNew))
                .appendQueryParameter("conditionUsed",String.valueOf(formData.conditionUsed))
                .appendQueryParameter("conditionUnspecified",String.valueOf(formData.conditionUnspecified))
                .appendQueryParameter("shippingLocal",String.valueOf(formData.shippingLocal))
                .appendQueryParameter("shippingFree",String.valueOf(formData.shippingFree))
                .appendQueryParameter("distance",String.valueOf(formData.distance))
                .appendQueryParameter("zipCode",formData.zipCode);
        return builder.build().toString();
    }

    public static String getItemDetailUrl(String itemId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority(BACKEND_URL)
                .appendPath("api")
                .appendPath("product")
                .appendPath(itemId);
        return builder.build().toString();
    }

    public static String getSimilarUrl(String itemId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority(BACKEND_URL)
                .appendPath("api")
                .appendPath("similar")
                .appendPath(itemId);
        return builder.build().toString();
    }

    public static String getPhotosUrl(String keyword) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority(BACKEND_URL)
                .appendPath("api")
                .appendPath("googlesearch")
                .appendQueryParameter("keyword",keyword);
        return builder.build().toString();
    }

    public static String getAutoCompleteUrl(String keyword) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority(BACKEND_URL)
                .appendPath("api")
                .appendPath("geosearch")
                .appendPath(keyword);
        return builder.build().toString();
    }

    public static String getIpApiUrl() {
        return  "http://ip-api.com/json";
    }


}
