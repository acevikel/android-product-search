package com.alpcevikel.productsearch.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtils {

    public static void itemAddToast(Context context, String itemTitle) {
        Toast toast = Toast.makeText(context,"Item "+itemTitle+" is added to wishlist",Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void itemRemoveToast(Context context, String itemTitle) {
        Toast toast = Toast.makeText(context,"Item "+itemTitle+" is removed from wishlist",Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void errorToast(Context context) {
        Toast toast = Toast.makeText(context,"Please fix all fields with erros",Toast.LENGTH_SHORT);
        toast.show();
    }
}
