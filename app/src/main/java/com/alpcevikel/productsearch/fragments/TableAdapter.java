package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.RecyclerViewClickListener;
import com.alpcevikel.productsearch.classes.CardData;
import com.alpcevikel.productsearch.utils.StringUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class TableAdapter extends RecyclerView.Adapter<TableAdapter.TableViewHolder> {
    private static final String TAG = "TableAdapter";
    private ArrayList<Pair<CardData,Boolean>> mDataset;
    private TableViewHolder mViewHolder;
    private Context parentContext;
    private RecyclerViewClickListener itemListener;
    public class TableViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView mTitleTextView;
        protected ImageView mImageView;
        protected ImageButton mWisbutton;
        protected TextView mzipText;
        protected TextView mshippingText;
        protected TextView mconditionText;
        protected TextView mpriceText;
        public View view;
        public TableViewHolder(View v) {

            super(v);
            view = v;
            v.setOnClickListener(this);
            mTitleTextView = (TextView) v.findViewById(R.id.textViewName);
            mImageView = (ImageView) v.findViewById(R.id.imageView);
            mWisbutton = (ImageButton) v.findViewById(R.id.wishButton);
            mzipText = (TextView) v.findViewById(R.id.textViewZip);
            mshippingText = (TextView) v.findViewById(R.id.textViewShipping);
            mconditionText = (TextView) v.findViewById(R.id.textViewStatus);
            mpriceText =  (TextView) v.findViewById(R.id.textViewPrice);
            // TODO check true background on init
            mWisbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.wishButtonClicked(v,getLayoutPosition(),mWisbutton);
                }
            });
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, getLayoutPosition());

        }
    }

    public TableAdapter(ArrayList<Pair<CardData,Boolean>> myDataset, RecyclerViewClickListener itemListener) {
        this.itemListener = itemListener;
        mDataset = myDataset;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public TableAdapter.TableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        parentContext = parent.getContext();
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);
        TableViewHolder vh = new TableViewHolder(v);
        return vh;
    }

    // TODO make some optimize to this by calling get once
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(TableViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        mViewHolder = holder;
        //holder.mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        holder.mTitleTextView.setText(StringUtils.cropString(mDataset.get(position).first.title,55).toUpperCase());
        holder.mconditionText.setText(mDataset.get(position).first.condition);
        holder.mpriceText.setText("$"+mDataset.get(position).first.price);
        holder.mzipText.setText("Zip: "+mDataset.get(position).first.zip);
        holder.mshippingText.setText(mDataset.get(position).first.shipping);
        Glide.with(parentContext).load(mDataset.get(position).first.imageUrl).into(holder.mImageView);
        if (mDataset.get(position).second){
            holder.mWisbutton.setBackgroundResource(R.drawable.cart_remove);
        } else {
            holder.mWisbutton.setBackgroundResource(R.drawable.cart_plus);
        }

    }

    // Return the size of your dataset (invoked by the l
    // ayout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static String getURLForResource (int resourceId) {
        return Uri.parse("android.resource://"+R.class.getPackage().getName()+"/" +resourceId).toString();
    }
}
