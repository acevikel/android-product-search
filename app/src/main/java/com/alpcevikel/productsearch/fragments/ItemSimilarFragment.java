package com.alpcevikel.productsearch.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.RecyclerViewClickListener;
import com.alpcevikel.productsearch.SimilarRecyclerListener;
import com.alpcevikel.productsearch.classes.ItemProductData;
import com.alpcevikel.productsearch.classes.SimilarCardData;
import com.alpcevikel.productsearch.utils.BackendUtils;
import com.alpcevikel.productsearch.utils.JSONUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

//TODO HANDLE NO SIMILAR ITEMS

public class ItemSimilarFragment extends Fragment implements SimilarRecyclerListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "ItemSimilarFragment";

    private String mitemId;
    private ArrayList<SimilarCardData> mDataset;
    private ArrayList<SimilarCardData> originalDataset;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Spinner mSortTypeSpinner;
    private Spinner mSortBySpinner;
    private String mSortTypeselected;
    private String mSortBySelected;
    private TextView fetchTextView;
    private ProgressBar fetchProgressbar;
    private LinearLayout resultLayout;
    private TextView mNoResultTextView;
    private boolean fetched;
    public ItemSimilarFragment() {
        // Required empty public constructor
    }

    public static ItemSimilarFragment newInstance(String param1) {
        ItemSimilarFragment fragment = new ItemSimilarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mDataset = new ArrayList<>();
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mitemId = getArguments().getString(ARG_PARAM1);
        }
        fetched = false;
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //String url = "http://192.168.1.7:3000/folder/similar.json";
        String url = BackendUtils.getSimilarUrl(mitemId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    updateDataset(jsonObject);
                    if (mDataset.size()==0) {
                        mNoResultTextView.setVisibility(View.VISIBLE);
                        mSortBySpinner.setEnabled(false);
                    }
                    originalDataset = new ArrayList<>(mDataset);
                    fetched = true;
                    setView();
                    Log.d(TAG, "onResponse: "+mDataset);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //testtxt.setText("Some erroorz");
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_item_similar, container, false);
        mSortBySpinner = (Spinner) rootView.findViewById(R.id.spinner_sortby);
        mSortTypeSpinner = (Spinner) rootView.findViewById(R.id.spinner_sorttype);
        fetchTextView = (TextView) rootView.findViewById(R.id.fetchInfoText);
        fetchProgressbar = (ProgressBar) rootView.findViewById(R.id.fetchProgressBar);
        resultLayout = (LinearLayout) rootView.findViewById(R.id.spinnerBar);
        mNoResultTextView = (TextView) rootView.findViewById(R.id.noSimilarItemsTextView);
        mSortTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                mSortTypeselected = selectedItem;
                Log.d(TAG, "onItemSelected: Selected item is "+selectedItem);
                updateLists();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSortBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                mSortBySelected = selectedItem;
                if (selectedItem.equals("Default")) {
                    resetDataset();
                    mSortTypeSpinner.setEnabled(false);
                } else {
                    updateLists();
                    mSortTypeSpinner.setEnabled(true);
                }

                Log.d(TAG, "onItemSelected: Selected by is : "+selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        recyclerView = (RecyclerView) rootView.findViewById(R.id.similar_recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new SimilarAdapter(mDataset,this);
        recyclerView.setAdapter(mAdapter);
        setView();
        return  rootView;
    }

    private void resetDataset() {
        if (mDataset!=null && originalDataset!=null) {
            mDataset.clear();
            for (SimilarCardData sd : originalDataset) {
                mDataset.add(sd);
            }
            mAdapter.notifyDataSetChanged();
            Log.d(TAG, "resetDataset: Dataset has been reset");
        }

    }

    @Override
    public void listItemClicked(View v, int position) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(mDataset.get(position).viewItemUrl));
        startActivity(i);

    }

    private void updateDataset(JSONObject jsonObject) {
        ArrayList<SimilarCardData>  newData = JSONUtils.parseSimilarCards(jsonObject);
        for (SimilarCardData similarCardData : newData) {
                mDataset.add(similarCardData);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void updateLists() {
        Collections.sort(mDataset,new SimilarProductComparator(mSortBySelected,mSortTypeselected));
        mAdapter.notifyDataSetChanged();
    }

    private void setView() {
        if (fetched) {
            fetchProgressbar.setVisibility(View.GONE);
            fetchTextView.setVisibility(View.GONE);
            resultLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}

class SimilarProductComparator implements Comparator<SimilarCardData> {

    private String mSortBy;
    private boolean ascendingSort;

    SimilarProductComparator(String sortBy, String sortType) {
        mSortBy = sortBy;
        ascendingSort = sortType.equals("Ascending");
    }

    @Override
    public int compare(SimilarCardData o1, SimilarCardData o2) {
        if (mSortBy.equals("Name")) {
            if (ascendingSort) {
                return o1.title.compareTo(o2.title);
            } else {
                return o2.title.compareTo(o1.title);
            }
        } else if (mSortBy.equals("Price")) {
            Float f1 = new Float(o1.price);
            Float f2 = new Float(o2.price);
            if (ascendingSort) {
                return f1.compareTo(f2);
            } else {
                return f2.compareTo(f1);
            }

        } else {
            Integer i1 = new Integer(o1.daysLeft);
            Integer i2 = new Integer(o2.daysLeft);
            if (ascendingSort) {
                return  i1.compareTo(i2);
            } else {
                return  i2.compareTo(i1);
            }
        }
    }
}
