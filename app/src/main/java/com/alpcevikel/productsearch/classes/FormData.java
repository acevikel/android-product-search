package com.alpcevikel.productsearch.classes;

import java.io.Serializable;

public class FormData implements Serializable {
    public String keyword;
    public int category;
    public boolean conditionNew;
    public boolean conditionUsed;
    public boolean conditionUnspecified;
    public boolean shippingLocal;
    public boolean shippingFree;
    public int distance;
    public String selectedOption;
    public String from;
    public String zipCode;

    public  FormData() {
        keyword = "iphone";
        category = 0;
        conditionNew = false;
        conditionUsed = false;
        conditionUnspecified = false;
        shippingLocal = false;
        shippingFree = false;
        distance = 10;
        selectedOption = "cur";
        from = "90007";
        zipCode = "90007";
    }

    @Override
    public String toString() {
        return "FormData{" +
                "keyword='" + keyword + '\'' +
                ", category=" + category +
                ", conditionNew=" + conditionNew +
                ", conditionUsed=" + conditionUsed +
                ", conditionUnspecified=" + conditionUnspecified +
                ", shippingLocal=" + shippingLocal +
                ", shippingFree=" + shippingFree +
                ", distance=" + distance +
                ", selectedOption='" + selectedOption + '\'' +
                ", from='" + from + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setConditionNew(boolean conditionNew) {
        this.conditionNew = conditionNew;
    }

    public void setConditionUsed(boolean conditionUsed) {
        this.conditionUsed = conditionUsed;
    }

    public void setConditionUnspecified(boolean conditionUnspecified) {
        this.conditionUnspecified = conditionUnspecified;
    }

    public void setShippingLocal(boolean shippingLocal) {
        this.shippingLocal = shippingLocal;
    }

    public void setShippingFree(boolean shippingFree) {
        this.shippingFree = shippingFree;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
