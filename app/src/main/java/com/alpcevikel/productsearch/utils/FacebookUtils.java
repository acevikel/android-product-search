package com.alpcevikel.productsearch.utils;

import android.content.Intent;
import android.net.Uri;

public class FacebookUtils {
    private static final String APP_ID = "401020720734083";

    public static String getUri(String href, String title,String price) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("http")
                .authority("www.facebook.com")
                .appendPath("dialog")
                .appendPath("share")
                .appendQueryParameter("app_id", APP_ID)
                .appendQueryParameter("href", href)
                .appendQueryParameter("quote","Buy "+title+" for $"+price+" from Ebay!")
                .appendQueryParameter("hashtag","#CSCI571Spring2019Ebay");
        return builder.build().toString();
    }
}
