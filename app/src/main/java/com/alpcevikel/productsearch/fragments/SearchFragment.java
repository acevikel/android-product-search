package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.ResultTableActivity;
import com.alpcevikel.productsearch.classes.FormData;
import com.alpcevikel.productsearch.classes.ItemShippingData;
import com.alpcevikel.productsearch.utils.BackendUtils;
import com.alpcevikel.productsearch.utils.JSONUtils;
import com.alpcevikel.productsearch.utils.StringUtils;
import com.alpcevikel.productsearch.utils.ToastUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {
    private  static final String TAG = "Sometag";
    private CheckBox nearbyCheckbox;
    private RadioGroup radioGroup;
    private TextView textViewFrom;
    private EditText editTextZip;
    private Button submitButton;
    private Button clearButton;
    private TextView warningZip;
    private TextView warningKeyword;
    private EditText inputKeyword;
    private AutoCompleteTextView inputZip;
    private TextView textViewCurLoc;
    private RelativeLayout fromLayout;
    private CheckBox checkBoxNew;
    private CheckBox checkBoxUsed;
    private CheckBox checkBoxUns;
    private CheckBox checkBoxLocal;
    private CheckBox checkBoxFreeship;
    private Spinner categorySpinner;
    private EditText inputMiles;
    private boolean nearbySearchEnabled;
    private RadioButton radioNearby;
    private RadioButton radioZipcode;
    private Context mContext;
    private List<String> autoCompleteList;
    private ArrayAdapter<String> mAdapter;
    private ArrayList<String> mDataset;
    private boolean hax;
    private String mZipCode;
    //TODO remove warning when new text typed



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment,container,false);
        getLocation();
        categorySpinner = (Spinner) view.findViewById(R.id.spinner);
        mContext = getContext();
        nearbySearchEnabled = false;
        hax = true;
        inputMiles = (EditText) view.findViewById(R.id.editText3);
        radioNearby = (RadioButton) view.findViewById(R.id.radioButton3);
        radioZipcode = (RadioButton) view.findViewById(R.id.radioButton4);
        checkBoxNew = (CheckBox) view.findViewById(R.id.checkBox);
        checkBoxUsed = (CheckBox) view.findViewById(R.id.checkBox2);
        checkBoxUns = (CheckBox) view.findViewById(R.id.checkBox3);
        checkBoxLocal = (CheckBox) view.findViewById(R.id.checkBox4);
        checkBoxFreeship = (CheckBox) view.findViewById(R.id.checkBoxD);
        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        textViewFrom = (TextView) view.findViewById(R.id.textView6);
        textViewCurLoc = (TextView) view.findViewById(R.id.currentLoctext);
        editTextZip = (EditText) view.findViewById(R.id.editText3);
        warningZip = (TextView) view.findViewById(R.id.warningZip);
        fromLayout = (RelativeLayout) view.findViewById(R.id.fromLayout);
        warningKeyword = (TextView) view.findViewById(R.id.warningKeyword);
        nearbyCheckbox = (CheckBox) view.findViewById(R.id.checkBox6);
        inputKeyword = (EditText) view.findViewById(R.id.editText2);
        inputZip = (AutoCompleteTextView) view.findViewById(R.id.editText4);
        nearbyCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CompoundButton) v).isChecked()){
                    setNearbyVisibility(true);
                } else {
                    setNearbyVisibility(false);
                }
            }
        });
        submitButton = (Button) view.findViewById(R.id.button);
        clearButton = (Button) view.findViewById(R.id.button2);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmit();
            }
        });
        clearButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClear();
            }
        }) );
        mDataset = new ArrayList<String>();
        mAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, mDataset);
        inputZip.setAdapter(mAdapter);
        inputZip.setThreshold(1);
        inputZip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (hax) {
                    Log.d(TAG, "onTextChanged: updated");
                    String written = inputZip.getText().toString();
                    if (written.length()>=1 && written.length()!=5) {
                        updateAutoCompleteList();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private void updateAutoCompleteList() {
        mDataset.clear();
        mAdapter.notifyDataSetChanged();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = BackendUtils.getAutoCompleteUrl(inputZip.getText().toString());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONUtils.fillAdapter(mAdapter,jsonObject);
                    mAdapter.notifyDataSetChanged();
                    hax = false;
                    inputZip.setText(inputZip.getText().toString());
                    inputZip.setSelection(inputZip.getText().length());
                    hax = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //testtxt.setText("Some erroorz");
            }
        });
        queue.add(stringRequest);
    }

    private void setNearbyVisibility(boolean enable) {
        if (enable) {
            inputZip.setVisibility(View.VISIBLE);
            radioGroup.setVisibility(View.VISIBLE);
            textViewFrom.setVisibility(View.VISIBLE);
            editTextZip.setVisibility(View.VISIBLE);
            textViewCurLoc.setVisibility(View.VISIBLE);
            fromLayout.setVisibility(View.VISIBLE);
            nearbySearchEnabled = true;
        } else {
            inputZip.setVisibility(View.GONE);
            radioGroup.setVisibility(View.GONE);
            textViewFrom.setVisibility(View.GONE);
            editTextZip.setVisibility(View.GONE);
            textViewCurLoc.setVisibility(View.GONE);
            fromLayout.setVisibility(View.GONE);
            nearbySearchEnabled = false;
        }
    }

    private boolean validateFields() {
        //TODO regex test here + toast
        boolean result = true;
        if (!StringUtils.testKeyword(inputKeyword.getText().toString())) {
            warningKeyword.setVisibility(View.VISIBLE);
            result = false;
        } else {
            warningKeyword.setVisibility(View.GONE);
        }

        //TODO another regex test here + toast
        if (radioZipcode.isChecked() && nearbySearchEnabled && !StringUtils.testZip(inputZip.getText().toString())) {
            warningZip.setVisibility(View.VISIBLE);
            result = false;
        } else {
            warningZip.setVisibility(View.GONE);
        }
        if (!result) ToastUtils.errorToast(mContext);
        return  result;
    }

    public void handleClear() {
        //Log.d(TAG, "handleClear: Selected position "+categorySpinner.getSelectedItemPosition());
        //Log.d(TAG, "handleClear: Selected radio id" + radioGroup.getCheckedRadioButtonId());
        radioNearby.toggle();
        categorySpinner.setSelection(0);
        warningZip.setVisibility(View.GONE);
        warningKeyword.setVisibility(View.GONE);
        inputKeyword.setText("");
        inputMiles.setText("");
        inputZip.setText("");
        checkBoxUsed.setChecked(false);
        checkBoxNew.setChecked(false);
        checkBoxUns.setChecked(false);
        checkBoxLocal.setChecked(false);
        checkBoxFreeship.setChecked(false);
        if (nearbyCheckbox.isChecked()) {
            nearbyCheckbox.setChecked(false);
            setNearbyVisibility(false);
        }

    }

    public void handleSubmit() {
        if (validateFields()) {
            openResulTableActivity();
        }
    }
    public void openResulTableActivity() {
        Intent intent = new Intent(getContext(), ResultTableActivity.class);
        FormData fd =getFormData();
        // TODO Populate form data.
        intent.putExtra("test",fd);
        startActivity(intent);
    }


    private FormData getFormData() {

        FormData formData = new FormData();
        formData.setKeyword(inputKeyword.getText().toString());
        formData.setCategory(this.categorySpinner.getSelectedItemPosition());
        formData.setConditionNew(checkBoxNew.isChecked());
        formData.setConditionUsed(checkBoxUsed.isChecked());
        formData.setConditionUnspecified(checkBoxUns.isChecked());
        formData.setShippingFree(checkBoxFreeship.isChecked());
        formData.setShippingLocal(checkBoxLocal.isChecked());

        // aim of here onward is to set zip and distance
        if (nearbyCheckbox.isChecked()) {
            if (radioNearby.isChecked()) {
                formData.setZipCode(mZipCode);
            } else {
                formData.setZipCode(inputZip.getText().toString());
            }
            String dist = inputMiles.getText().toString();
            if (dist.length()!=0) {
                formData.setDistance(Integer.parseInt(dist));
            } else {
                formData.setDistance(0);
            }

        } else {
            formData.setZipCode("-1");
            formData.setDistance(0);
        }
        return formData;
    }

    private void getLocation() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = BackendUtils.getIpApiUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    mZipCode = jsonObject.getString("zip");
                    Log.d(TAG, "Location succesfully recieved as : " + mZipCode);
                } catch (Exception e) {
                    Log.d(TAG, "Location fetch failed: ");
                    mZipCode = "90007";
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //testtxt.setText("Some erroorz");
            }
        });
        queue.add(stringRequest);
    }




}
