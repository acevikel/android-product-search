package com.alpcevikel.productsearch.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    public static String cropString(String s, int maxlen) {
        if (s.length()<=maxlen){
            return s;
        } else {
            return s.substring(0,maxlen)+"...";
        }
    }

    public static boolean testKeyword(String keyword) {
        Pattern p = Pattern.compile("[a-zA-Z0-9]+");
        Matcher m = p.matcher(keyword);
        return  m.find();
    }

    public static boolean testZip(String zipcode) {
        return zipcode.matches("[0-9]{5}");
    }

    public static String wrapString(String s, String deliminator, int length) {
        String result = "";
        int lastdelimPos = 0;
        for (String token : s.split(" ", -1)) {
            if (result.length() - lastdelimPos + token.length() > length) {
                result = result + deliminator + token;
                lastdelimPos = result.length() + 1;
            }
            else {
                result += (result.isEmpty() ? "" : " ") + token;
            }
        }
        return result;
    }
}
