package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.alpcevikel.productsearch.ItemDetailActivity;
import com.alpcevikel.productsearch.LocalStorageManager;
import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.RecyclerViewClickListener;
import com.alpcevikel.productsearch.classes.CardData;
import com.alpcevikel.productsearch.classes.CombinedData;
import com.alpcevikel.productsearch.utils.ToastUtils;

import java.util.ArrayList;


public class TableFragment extends Fragment  implements RecyclerViewClickListener {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private View view;
    private ArrayList<Pair<CardData,Boolean>> mDataset;
    private static final String TAG = "TableFragment";
    private LocalStorageManager localStorageManager;
    public TableFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_table, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mDataset = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new TableAdapter(mDataset, this);
        recyclerView.setAdapter(mAdapter);
        localStorageManager = new LocalStorageManager(getContext());
        return view;
    }

    public void initRecyclerViewWithBool(ArrayList<Pair<CardData,Boolean>> inp) {
        mDataset.clear();
        for (Pair<CardData,Boolean> pr : inp) {
            mDataset.add(pr);
        }
        mAdapter.notifyDataSetChanged();
    }

    public void initRecyclerView (ArrayList<CardData> inputData) {
        for (CardData item : inputData) {
            mDataset.add(Pair.create(item,localStorageManager.hasItem(item.itemId)));
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Log.d(TAG, "recyclerViewListClicked: Clicked on item:" +position);
        openItemDetailActivity(position);
    }

    @Override
    public void wishButtonClicked(View v, int position, ImageButton imageButton) {
        Context mContext = getContext();
        String activityname = getActivity().getClass().getSimpleName();
        Log.d(TAG, "wishButtonClicked: Called from "+activityname);
        CardData cd = mDataset.get(position).first;
        boolean isWished = mDataset.get(position).second;
        if (isWished) {
            ToastUtils.itemRemoveToast(mContext,cd.title);
            localStorageManager.removeObject(cd.itemId);
            if (activityname.equals("MainActivity")) {
                mDataset.remove(position);
                notifyParentToUpdate();
            } else {
                mDataset.set(position,new Pair<CardData, Boolean>(cd,Boolean.FALSE));
            }
        } else {
            ToastUtils.itemAddToast(mContext,cd.title);
            localStorageManager.putObject(cd);
            mDataset.set(position,new Pair<CardData, Boolean>(cd,Boolean.TRUE));
        }
        mAdapter.notifyDataSetChanged();
    }

    public void notifyParentToUpdate(){
        WishListFragment parentFrag = ((WishListFragment) TableFragment.this.getParentFragment());
        parentFrag.updateTotalText();
    }

    public ArrayList<Pair<CardData, Boolean>> getmDataset() {
        return mDataset;
    }

    public void openItemDetailActivity(int position) {
        Intent intent = new Intent(getContext(), ItemDetailActivity.class);
        Pair<CardData,Boolean> showData = mDataset.get(position);
        CombinedData cd = new CombinedData(showData);
        intent.putExtra("test", cd);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        handleDatasetIntegrity();

    }

    private void handleDatasetIntegrity() {
        String actname = getActivity().getClass().getSimpleName();
        if (actname.equals("ResultTableActivity")) {
            for (int i=0;i<mDataset.size();i++) {
                Pair<CardData,Boolean> pr = mDataset.get(i);

                boolean datastatus = pr.second;
                boolean realstatus = localStorageManager.hasItem(pr.first.itemId);
                if (realstatus!=datastatus) {
                    mDataset.set(i,new Pair<CardData, Boolean>(pr.first,realstatus));
                }
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
