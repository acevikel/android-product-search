package com.alpcevikel.productsearch;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.classes.CardData;
import com.alpcevikel.productsearch.classes.FormData;
import com.alpcevikel.productsearch.fragments.TableFragment;
import com.alpcevikel.productsearch.utils.BackendUtils;
import com.alpcevikel.productsearch.utils.JSONUtils;
import com.alpcevikel.productsearch.utils.StringUtils;
import com.alpcevikel.productsearch.utils.ToastUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultTableActivity extends AppCompatActivity {

    private static final String TAG = "ResultTableActivity";
    private Intent intent;
    private TableFragment tableFragment;
    private ProgressBar progressBar;
    private TextView fetchInfoTextView;
    private LinearLayout infoBar;
    private TextView infoItemcount;
    private TextView infoKeyword;
    private FormData formData;
    private TextView notFoundTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_table);
        tableFragment = (TableFragment) getSupportFragmentManager().findFragmentById(R.id.tableFragmentx);
        progressBar = (ProgressBar) findViewById(R.id.indeterminateBar);
        fetchInfoTextView = (TextView) findViewById(R.id.fetchInfoText);
        infoBar = (LinearLayout) findViewById(R.id.infoBar);
        infoItemcount = (TextView) findViewById(R.id.textView8);
        infoKeyword = (TextView) findViewById(R.id.textView10);
        notFoundTextView = (TextView) findViewById(R.id.notFoundTextView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getColor(R.color.color_purple_bg)
        ));
        intent = getIntent();
        formData = (FormData) intent.getSerializableExtra("test");
        Log.d(TAG, "onCreate: " +formData);;
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = BackendUtils.getAdvancedSearchUrl(formData);
        Log.d(TAG, "onCreate: URL IS "+url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ArrayList<CardData> resultList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    boolean hasData = JSONUtils.searchHasResults(jsonObject);
                    if (hasData) {
                        JSONObject jx = jsonObject.getJSONArray("findItemsAdvancedResponse").getJSONObject(0);
                        JSONObject jy = jx.getJSONArray("searchResult").getJSONObject(0);
                        JSONArray jz = jy.getJSONArray("item");
                        for (int i=0;i<jz.length();i++) {
                            JSONObject obj = jz.getJSONObject(i);
                            resultList.add(new CardData(obj));
                        }
                        tableFragment.initRecyclerView(resultList);
                        infoBar.setVisibility(View.VISIBLE);
                        infoKeyword.setText(formData.keyword);
                        infoItemcount.setText(String.valueOf(resultList.size()));
                    } else {
                        Log.d(TAG, "onResponse: NO RESULTS FOUND");
                        notFoundTextView.setVisibility(View.VISIBLE);
                    }
                    progressBar.setVisibility(View.GONE);
                    fetchInfoTextView.setVisibility(View.GONE);
                } catch (Exception e) {
                    Log.d(TAG, "onResponse: fetching failed");
                    progressBar.setVisibility(View.GONE);
                    fetchInfoTextView.setVisibility(View.GONE);
                    notFoundTextView.setVisibility(View.VISIBLE);
                    notFoundTextView.setText("Data Fetching Error");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //TODO do this
                progressBar.setVisibility(View.GONE);
                fetchInfoTextView.setVisibility(View.GONE);
                notFoundTextView.setVisibility(View.VISIBLE);
                notFoundTextView.setText("Data Fetching Error");
                ToastUtils.errorToast(getApplicationContext());
            }
        });
        queue.add(stringRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
