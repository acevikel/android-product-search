package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alpcevikel.productsearch.LocalStorageManager;
import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.classes.CardData;


public class WishListFragment extends Fragment {
    private static final String TAG = "WishListFragment";
    private TableFragment tableFragment;
    private TextView totalText;
    private TextView expText;
    private LocalStorageManager localStorageManager;
    private View fragmentView;
    private TextView noResultsTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wishlist_fragment,container,false);
        tableFragment = (TableFragment) getChildFragmentManager().findFragmentById(R.id.wishTableFragment);
        fragmentView = (View) view.findViewById(R.id.wishTableFragment);
        noResultsTextView = (TextView) view.findViewById(R.id.noResultsTextView);
        Context context = getContext();
        localStorageManager = new LocalStorageManager(context);
        tableFragment.initRecyclerViewWithBool(localStorageManager.getDataset());
        totalText = (TextView) view.findViewById(R.id.totalTextView);
        expText = (TextView) view.findViewById(R.id.totalExpView);
        updateTotalText();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: Activity started");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: Activity resumed");
        tableFragment.initRecyclerViewWithBool(localStorageManager.getDataset());
        updateTotalText();
    }

    public void updateTotalText() {
        float total = 0;
        int count = 0;
        for (Pair<CardData,Boolean> item : tableFragment.getmDataset()) {
            count++;
            total+=Float.parseFloat(item.first.price);
        }
        this.expText.setText("Wishlist Total ("+String.valueOf(count)+" items):");
        this.totalText.setText("$"+String.valueOf(total));
        if (count==0) {
            this.fragmentView.setVisibility(View.GONE);
            this.noResultsTextView.setVisibility(View.VISIBLE);
        } else {
            this.fragmentView.setVisibility(View.VISIBLE);
            this.noResultsTextView.setVisibility(View.GONE);
        }
    }
}
