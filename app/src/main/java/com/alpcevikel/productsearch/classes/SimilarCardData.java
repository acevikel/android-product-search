package com.alpcevikel.productsearch.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class SimilarCardData {
    public String imageUrl;
    public String title;
    public double shippingCost;
    public int daysLeft;
    public double price;
    public String viewItemUrl;

    public  SimilarCardData(JSONObject row) throws JSONException {
        this.imageUrl = row.getString("imageURL");
        this.title = row.getString("title");
        this.shippingCost = Double.parseDouble(row.getJSONObject("shippingCost").getString("__value__"));
        this.daysLeft = parseDaysLeft(row.getString("timeLeft"));
        this.price = Double.parseDouble(row.getJSONObject("buyItNowPrice").getString("__value__"));
        this.viewItemUrl = row.getString("viewItemURL");
    }

    private  int parseDaysLeft(String days) {
        String res = days.substring(days.indexOf("P") + 1);
        res = res.substring(0, res.indexOf("D"));
        return Integer.parseInt(res);
    }

    @Override
    public String toString() {
        return "SimilarCardData{" +
                "imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", shippingCost=" + shippingCost +
                ", daysLeft=" + daysLeft +
                ", price=" + price +
                ", viewItemUrl='" + viewItemUrl + '\'' +
                '}';
    }
}
