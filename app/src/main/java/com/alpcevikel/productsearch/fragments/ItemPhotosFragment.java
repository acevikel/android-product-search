package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.classes.ItemShippingData;
import com.alpcevikel.productsearch.utils.BackendUtils;
import com.alpcevikel.productsearch.utils.JSONUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;

//TODO HANDLE NO PHOTOS

public class ItemPhotosFragment extends Fragment {
    private static final String TAG = "ItemPhotosFragment";
    private static final String ARG_PARAM1 = "param1";
    private View mView;
    private Context mContext;
    private LinearLayout mimageLayout;
    private TextView fetchTextView;
    private ProgressBar fetchProgressbar;
    private ArrayList<String> photosObj;
    boolean fetched;
    private String mItemTitle;

    public ItemPhotosFragment() {
        // Required empty public constructor
    }

    public static ItemPhotosFragment newInstance(String param1) {
        ItemPhotosFragment fragment = new ItemPhotosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemTitle = getArguments().getString(ARG_PARAM1);
        }
        fetched = false;
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //String url = "http://192.168.1.7:3000/folder/grsc.json";
        String url = BackendUtils.getPhotosUrl(mItemTitle);
        //TODO fix non searchable items on string using regex
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    photosObj = JSONUtils.getImageList(jsonObject);
                    fetched = true;
                    setView();
                    Log.d(TAG, "onResponse: " + photosObj);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //testtxt.setText("Some erroorz");
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_item_photos, container, false);
        mView = rootView;
        mContext = getContext();
        mimageLayout = (LinearLayout) rootView.findViewById(R.id.imageContainer);
        fetchTextView = (TextView) rootView.findViewById(R.id.fetchInfoText);
        fetchProgressbar = (ProgressBar) rootView.findViewById(R.id.fetchProgressBar);
        setView();
        return  rootView;
    }

    private void addImages(ArrayList<String> images) {
        //TODO add no images if no images
        for (String img : images) {
            ImageView imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.FIT_END);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(layoutParams);
            Glide.with(mContext).load(img).into(imageView);
            mimageLayout.addView(imageView);
        }
    }

    private void setView() {
        if (fetched) {
            addImages(photosObj);
            fetchProgressbar.setVisibility(View.GONE);
            fetchTextView.setVisibility(View.GONE);
        }
    }

}
