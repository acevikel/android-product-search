package com.alpcevikel.productsearch;

import android.view.View;
import android.widget.ImageButton;


public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position);
    public  void  wishButtonClicked(View v, int position, ImageButton bt);
}