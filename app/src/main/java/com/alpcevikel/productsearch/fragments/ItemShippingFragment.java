package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.classes.ItemShippingData;
import com.alpcevikel.productsearch.utils.BackendUtils;
import com.alpcevikel.productsearch.utils.TableCreator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;


public class ItemShippingFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "ItemShippingFragment";
    private String mitemId;
    private String mShippingCost;
    private View mView;
    private Context mContext;
    private LinearLayout mShippingLayout;
    private TextView fetchTextView;
    private ProgressBar fetchProgressbar;
    private ItemShippingData shippingObj;
    private boolean fetched;
    public ItemShippingFragment() {
        // Required empty public constructor
    }
    public static ItemShippingFragment newInstance(String param1, String param2) {
        ItemShippingFragment fragment = new ItemShippingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mitemId = getArguments().getString(ARG_PARAM1);
            mShippingCost = getArguments().getString(ARG_PARAM2);
        }
        fetched = false;
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //String url = "http://192.168.1.7:3000/folder/detail2.json";
        String url = BackendUtils.getItemDetailUrl(mitemId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    shippingObj = new ItemShippingData(jsonObject,mShippingCost);
                    fetched = true;
                    setView();
                    Log.d(TAG, "onResponse: " + shippingObj);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //testtxt.setText("Some erroorz");
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_item_shipping, container, false);
        mView = rootView;
        mContext = getContext();
        mShippingLayout  = (LinearLayout) rootView.findViewById(R.id.shippingLayout);
        fetchTextView = (TextView) rootView.findViewById(R.id.fetchInfoText);
        fetchProgressbar = (ProgressBar) rootView.findViewById(R.id.fetchProgressBar);
        setView();
        return rootView;

    }

    private void setView() {
        if (fetched) {
            createTables(shippingObj);
            fetchProgressbar.setVisibility(View.GONE);
            fetchTextView.setVisibility(View.GONE);
        }
    }

    private void createTables(ItemShippingData shippingData) {
        TableCreator soldbyCreator = new TableCreator(mContext);
        soldbyCreator.setTextHeader("Sold By",R.drawable.shipping_truck);
        soldbyCreator.addLinkedText("Store Name",shippingData.storeName,shippingData.storeUrl);
        soldbyCreator.addKeyValueRow("Feedback Score",shippingData.feedbackScore);
        soldbyCreator.addCircularScore("Popularity",shippingData.feedbackPercent,mView);
        soldbyCreator.addStarRow("Feedback star",shippingData.feedbackScore);
        mShippingLayout.addView(soldbyCreator.getTableLayout());
        mShippingLayout.addView(TableCreator.createDivider(mContext));


        TableCreator shippingCreator = new TableCreator(mContext);
        shippingCreator.setTextHeader("Shipping Info",R.drawable.shipping_ferry);
        shippingCreator.addKeyValueRow("Shipping Cost",shippingData.shippingText);
        shippingCreator.addKeyValueRow("Global Shipping",shippingData.globalShipping);
        shippingCreator.addKeyValueRow("Handling Time",shippingData.handlingText);
        shippingCreator.addKeyValueRow("Condition",shippingData.condition);
        mShippingLayout.addView(shippingCreator.getTableLayout());
        mShippingLayout.addView(TableCreator.createDivider(mContext));

        TableCreator returnCreator = new TableCreator(mContext);
        returnCreator.setTextHeader("Return Policy",R.drawable.shipping_dump_truck);
        returnCreator.addKeyValueRow("Policy",shippingData.returnPolicy);
        returnCreator.addKeyValueRow("Returns within",shippingData.returnsWithin);
        returnCreator.addKeyValueRow("Refund Mode",shippingData.refundMode);
        returnCreator.addKeyValueRow("Shipped by",shippingData.shippedBy);
        mShippingLayout.addView(returnCreator.getTableLayout());

    }

}
