package com.alpcevikel.productsearch.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;

import com.alpcevikel.productsearch.R;

public class StarUtils {

    public static  Drawable  getStar(Context context, String score) {
        int scx = Integer.parseInt(score);
        int starcolor;
        int drawable_star;
        if (scx>=10000) {
            drawable_star = R.drawable.star_shooting;
        } else {
            drawable_star = R.drawable.star;
        }
        if (scx>=1000000) {
            starcolor = R.color.silver_star;
        } else if (scx>=500000) {
            starcolor = R.color.star_green;
        } else if (scx>=100000) {
            starcolor = R.color.red_star;
        } else if (scx>=50000) {
            starcolor = R.color.purple_star;
        } else if (scx>=25000) {
            starcolor = R.color.turqoise_star;
        } else if (scx>=10000) {
            starcolor = R.color.yellow_star;
        } else if (scx>=5000) {
            starcolor = R.color.star_green;
        } else if (scx>=1000) {
            starcolor = R.color.red_star;
        } else if (scx>=500) {
            starcolor = R.color.purple_star;
        } else if (scx>=100) {
            starcolor = R.color.turqoise_star;
        } else if (scx>=10) {
            starcolor = R.color.yellow_star;
        } else {
            starcolor = R.color.star_black;
        }
        Drawable drawable = ContextCompat.getDrawable(context, drawable_star);
        Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
        Drawable mutableDrawable = wrappedDrawable.mutate();
        DrawableCompat.setTint(mutableDrawable,ContextCompat.getColor(context,starcolor));
        return mutableDrawable;
    }
}
