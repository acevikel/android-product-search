package com.alpcevikel.productsearch.utils;

import android.util.Log;
import android.widget.Adapter;
import android.widget.ArrayAdapter;

import com.alpcevikel.productsearch.classes.SimilarCardData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONUtils {
    private static final String TAG="JSONUtils";

    public static ArrayList<String> getImageList(JSONObject jsonObject) {
        ArrayList<String> result = new ArrayList<>();
        try {
            JSONArray arr = jsonObject.getJSONArray("items");
            for (int i=0;i<arr.length();i++) {
                JSONObject obj = arr.getJSONObject(i);
                result.add(obj.getString("link"));
            }
        } catch (Exception e) {
            Log.d(TAG, "getImageList: Image fetch failed");
        }
        return  result;
    }

    public static ArrayList<SimilarCardData> parseSimilarCards (JSONObject object) {
        ArrayList<SimilarCardData> result = new ArrayList<>();
        try {
            JSONArray arr = object.getJSONObject("getSimilarItemsResponse").getJSONObject("itemRecommendations").getJSONArray("item");
            for (int i=0;i<arr.length();i++) {
                result.add(new SimilarCardData(arr.getJSONObject(i)));
            }
        } catch (Exception e) {
            Log.d(TAG, "parseSimilarCards: Failed to create data array");
            e.printStackTrace();
            return  result;
        }
        return  result;
    }

    public static  void  fillAdapter (ArrayAdapter<String> adapter, JSONObject object) {
        adapter.clear();
        try {
            JSONArray arr = object.getJSONArray("postalCodes");
            for (int i=0;i<arr.length();i++) {
                if (i>4) break;
                adapter.add(arr.getJSONObject(i).getString("postalCode"));
            }
        } catch (Exception e ) {

        }

    }

    public static  void  fillArrayList (ArrayList<String> list, JSONObject object) {
        list.clear();
        try {
            JSONArray arr = object.getJSONArray("postalCodes");
            for (int i=0;i<arr.length();i++) {
                list.add(arr.getJSONObject(i).getString("postalCode"));
            }
        } catch (Exception e ) {

        }

    }

    public static boolean searchHasResults(JSONObject object) {
        try {
            String strx = object.getJSONArray("findItemsAdvancedResponse").getJSONObject(0).getJSONArray("searchResult").getJSONObject(0).getString("@count");
            return !strx.equals("0");

        } catch (Exception e) {
            Log.d(TAG, "searchHasResults: FAILED TO FETCH RESULTS");
            return  false;
        }
    }
}
