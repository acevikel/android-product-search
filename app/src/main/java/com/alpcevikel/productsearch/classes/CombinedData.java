package com.alpcevikel.productsearch.classes;

import android.util.Pair;

import com.alpcevikel.productsearch.classes.CardData;

import java.io.Serializable;

public class CombinedData implements Serializable {
    private CardData cardData;
    private  Boolean aBoolean;

    public CombinedData (Pair<CardData,Boolean> cardDataBooleanPair)  {
        this.cardData = cardDataBooleanPair.first;
        this.aBoolean = cardDataBooleanPair.second;
    }

    public CardData getCardData() {
        return cardData;
    }

    public Boolean getaBoolean() {
        return aBoolean;
    }

    @Override
    public String toString() {
        return "CombinedData{" +
                "cardData=" + cardData +
                ", aBoolean=" + aBoolean +
                '}';
    }
}
