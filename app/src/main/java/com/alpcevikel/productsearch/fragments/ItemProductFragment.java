package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.classes.ItemProductData;
import com.alpcevikel.productsearch.utils.BackendUtils;
import com.alpcevikel.productsearch.utils.TableCreator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.util.ArrayList;


public class ItemProductFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String TAG = "ItemProductFragment";
    // TODO: Rename and change types of parameters
    private String mitemId;
    private String mshippingCost;
    private View mView;
    private LinearLayout mImages;
    private Context mContext;
    private LinearLayout mInformation;
    private ScrollView mScrollView;
    private TextView fetchTextView;
    private ProgressBar fetchProgressbar;
    private boolean fetched;
    ItemProductData productObj;

    public ItemProductFragment() {
        // Required empty public constructor
    }

    public static ItemProductFragment newInstance(String param1, String param2) {
        ItemProductFragment fragment = new ItemProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: CREATED product fragment");
        fetched = false;
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mitemId = getArguments().getString(ARG_PARAM1);
            mshippingCost = getArguments().getString(ARG_PARAM2);
        }
        RequestQueue queue = Volley.newRequestQueue(getContext());
        //String url = "http://192.168.1.7:3000/folder/detail2.json";
        String url = BackendUtils.getItemDetailUrl(mitemId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    productObj = new ItemProductData(jsonObject,mshippingCost);
                    fetched =true;
                    enableVisibility();
                    Log.d(TAG, "onResponse: " + productObj);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //testtxt.setText("Some erroorz");
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: CREATED view of product fragment");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_item_product, container, false);
        mView = rootView;
        mImages = (LinearLayout) rootView.findViewById(R.id.images_view);
        mContext = getContext();
        mInformation = (LinearLayout) rootView.findViewById(R.id.information_table);
        mScrollView = (ScrollView) rootView.findViewById(R.id.item_scroll_view);
        fetchTextView = (TextView) rootView.findViewById(R.id.fetchInfoText);
        fetchProgressbar = (ProgressBar) rootView.findViewById(R.id.fetchProgressBar);
        enableVisibility();
        return rootView;
    }

    private void createImageLayout(ArrayList<String> images) {
        for (String link : images) {
            ImageView imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.FIT_START);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 700);
            imageView.setLayoutParams(layoutParams);
            Glide.with(mContext).load(link).into(imageView);
            mImages.addView(imageView);
        }
    }

    private void createTextInfo(ItemProductData data) {
        TextView tv = new TextView(mContext);
        tv.setText(data.title);
        tv.setTextSize(25);
        tv.setPadding(50,100,0,0);
        tv.setTypeface(null,Typeface.BOLD);
        mInformation.addView(tv);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0,30,0,0);
        lp.setLayoutDirection(LinearLayout.HORIZONTAL);
        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setLayoutParams(lp);

        TextView priceView = new TextView(mContext);
        priceView.setText("$"+data.price);
        priceView.setTextColor(getResources().getColor(R.color.color_purple_bg));
        priceView.setTypeface(null, Typeface.BOLD);
        priceView.setPadding(50,0,0,0);
        linearLayout.addView(priceView);

        TextView shipinfo = new TextView(mContext);
        shipinfo.setText(data.shippingText);
        shipinfo.setPadding(20,0,0,0);
        linearLayout.addView(shipinfo);

        mInformation.addView(linearLayout)
        ;
    }

    private void createTables(ItemProductData data) {

        TableCreator infoCreator = new TableCreator(getContext());
        mInformation.addView(TableCreator.createDivider(mContext));
        infoCreator.setTextHeader("Highlights",R.drawable.information);
        infoCreator.addKeyValueRow("Subtitle",data.subtitle);
        infoCreator.addKeyValueRow("Price",data.price);
        infoCreator.addKeyValueRow("Brand",data.brand);
        mInformation.addView(infoCreator.getTableLayout());

        mInformation.addView(TableCreator.createDivider(mContext));
        TableCreator tc = new TableCreator(getContext());
        tc.setTextHeader("Specifications",R.drawable.wrench);
        for (String s : data.specifics) {
            tc.addBulletedRow(s);
        }
        TableLayout tl  = tc.getTableLayout();
        mInformation.addView(tl);

    }

    private void putViews() {
        createImageLayout(productObj.pictures);
        createTextInfo(productObj);
        createTables(productObj);
    }
    private void enableVisibility() {
        if (fetched) {
            fetchProgressbar.setVisibility(View.GONE);
            fetchTextView.setVisibility(View.GONE);
            putViews();
        }
    }


}
