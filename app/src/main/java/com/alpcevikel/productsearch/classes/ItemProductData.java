package com.alpcevikel.productsearch.classes;

import android.util.Log;

import com.alpcevikel.productsearch.utils.TableCreator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ItemProductData {
    private static final String TAG = "ItemProductData";
    public static final String  ITEM_NOT_EXIST = "PRODUCTNOTAVAILABLE";
    public ArrayList<String> pictures;
    public String title;
    public String price;
    public String shippingPrice;
    public String shippingText; //if cost 0 With Free shipping else With XXX shipping
    public String brand;
    public String subtitle;
    public ArrayList<String> specifics;  //first must be brand

    public ItemProductData() {

    }

    public ItemProductData(JSONObject object, String shippingPrice) {
        this.shippingPrice = shippingPrice;
        try {
            JSONObject item = object.getJSONObject("Item");
            setPictures(item);
            setTitle(item);
            setSubtitle(item);
            setPrice(item);
            setShippingText();
            setBrand(item);
            setSpecifics(item);

        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    private void setPictures(JSONObject object) {
        this.pictures = new ArrayList<>();
        try {
            JSONArray pics = object.getJSONArray("PictureURL");
            for (int i=0;i<pics.length();i++) {
                pictures.add(pics.getString(i));
            }
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    public void setTitle(JSONObject object) {
        try {
            this.title = object.getString("Title");
        } catch (Exception e ) {
            e.printStackTrace();
            this.title = ITEM_NOT_EXIST;
        }
    }

    public void setSubtitle(JSONObject object) {
        try {
            this.subtitle = object.getString("Subtitle");
        } catch (Exception e ) {
            e.printStackTrace();
            this.subtitle = ITEM_NOT_EXIST;
        }
    }

    public void setPrice(JSONObject object) {
        if (object.has("ConvertedCurrentPrice")){
            try {
                 this.price = object.getJSONObject("ConvertedCurrentPrice").getString("Value");
            } catch (Exception e) {
                e.printStackTrace();
                this.price = ITEM_NOT_EXIST;
            }


        } else if (object.has("CurrentPrice")) {
            try {
                this.price = object.getJSONObject("CurrentPrice").getString("Value");
            } catch (Exception e) {
                e.printStackTrace();
                this.price = ITEM_NOT_EXIST;
            }

        } else {
            this.price = ITEM_NOT_EXIST;
        }

    }


    public void setShippingText() {
        if (this.shippingPrice.equals("N/A")) {
            this.shippingText = "";
            return;
        }
        if (Float.parseFloat(this.shippingPrice)==0) {
            this.shippingText = "With Free Shipping";
        } else {
            this.shippingText = "With $"+this.shippingPrice+" shipping";
        }
    }

    public void setBrand(JSONObject object) {

        try {
            JSONArray arr = object.getJSONObject("ItemSpecifics").getJSONArray("NameValueList");
            for (int i =0;i< arr.length();i++) {
                if (arr.getJSONObject(i).getString("Name").equals("Brand")) {
                    this.brand = arr.getJSONObject(i).getJSONArray("Value").getString(0);
                    break;
                }
                this.brand = ITEM_NOT_EXIST;
            }
        } catch (Exception e ) {
            e.printStackTrace();
            this.brand = ITEM_NOT_EXIST;
        }
    }

    public void setSpecifics(JSONObject object) {
        this.specifics = new ArrayList<>();
        if (!this.brand.equals(ITEM_NOT_EXIST)) {
            this.specifics.add(this.brand);
        }
        try {
            JSONArray arr = object.getJSONObject("ItemSpecifics").getJSONArray("NameValueList");
            for (int i =0;i< arr.length();i++) {
                if (arr.getJSONObject(i).getString("Name").equals("Brand")) {
                    continue;
                } else {
                    this.specifics.add(arr.getJSONObject(i).getJSONArray("Value").getString(0));
                }
            }
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "ItemProductData{" +
                "pictures=" + pictures +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", shippingPrice='" + shippingPrice + '\'' +
                ", shippingText='" + shippingText + '\'' +
                ", brand='" + brand + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", specifics=" + specifics +
                '}';
    }
}


