package com.alpcevikel.productsearch.classes;
import com.alpcevikel.productsearch.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.Serializable;


public class CardData implements Serializable {
    private static final String TAG = "CardData";
    public String imageUrl;
    public String title;
    public String price;
    public String shipping;
    public String zip;
    public String itemId;
    public String shippingCost;
    public String condition;
    public String shareUrl;

    public  CardData() {
        this.imageUrl = "";
        this.title = "";
        this.price = "";
        this.shipping = "";
        this.zip = "";
        this.itemId = "";
        this.shippingCost = "";
        this.condition = "";
        this.shareUrl = "";
    }

    public  CardData(JSONObject row) {
        setImageUrl(row);
        setTitle(row);
        setCondition(row);
        setItemId(row);
        setPrice(row);
        //setShipping(row);
        setShippingCost(row);
        setZip(row);
        setShareUrl(row);
    }

    //TODO fix default values
    public void setImageUrl(JSONObject jsonObject) {
        try {
            this.imageUrl = (String) jsonObject.getJSONArray("galleryURL").get(0);
         } catch (Exception e ){
            this.imageUrl = "http://thumbs1.ebaystatic.com/pict/04040_0.jpg";
        }
    }

    public void setTitle(JSONObject jsonObject) {
        try {
            this.title = (String) jsonObject.getJSONArray("title").get(0);
        } catch (Exception e ){
            this.title = "N/A";
        }
    }

    public void setPrice(JSONObject jsonObject) {
        try {
            JSONArray j1 = jsonObject.getJSONArray("sellingStatus").getJSONObject(0).getJSONArray("currentPrice");
            this.price =  j1.getJSONObject(0).getString("__value__");
        } catch (Exception e ){
            this.price = "N/A";
        }
    }

    public void setShipping(JSONObject jsonObject) {
        try {
            this.shipping = (String) jsonObject.getJSONArray("shippingInfo").getJSONObject(0).getJSONArray("shippingType").get(0);
        } catch (Exception e ){
            this.shipping = "N/A";
        }

    }

    public void setZip(JSONObject jsonObject) {
        try {
            this.zip = jsonObject.getJSONArray("postalCode").getString(0);
        } catch (Exception e ) {
            this.zip = "N/A";
        }
    }

    public void setItemId(JSONObject jsonObject) {
        try {
            this.itemId = (String) jsonObject.getJSONArray("itemId").get(0);
        } catch (Exception e ){
            this.itemId = "N/A";
        }
    }

    public void setShippingCost(JSONObject jsonObject) {
        try {
            JSONArray j1 = jsonObject.getJSONArray("shippingInfo").getJSONObject(0).getJSONArray("shippingServiceCost");
            this.shippingCost =  j1.getJSONObject(0).getString("__value__");
            if (this.shippingCost.equals("0.0")) {
                this.shipping = "Free Shipping";
            } else {
                this.shipping = "$"+this.shippingCost + " Shipping";
            }
        } catch (Exception e ){
            this.shippingCost = "N/A";
            this.shipping = "N/A";
        }
    }

    public void setCondition(JSONObject jsonObject) {
        try {
            String unwrapped = (String) jsonObject.getJSONArray("condition").getJSONObject(0).getJSONArray("conditionDisplayName").get(0);
            this.condition = StringUtils.wrapString(unwrapped,"\n",12);
        } catch (Exception e ){
            this.condition = "N/A";
        }
    }

    public void setShareUrl(JSONObject jsonObject) {
        try {
            this.shareUrl = jsonObject.getJSONArray("viewItemURL").getString(0);
        } catch (Exception e ){
            this.shareUrl = "www.ebay.com";
        }
    }

    @Override
    public String toString() {
        return "CardData{" +
                "imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", shipping='" + shipping + '\'' +
                ", zip='" + zip + '\'' +
                ", itemId='" + itemId + '\'' +
                ", shippingCost='" + shippingCost + '\'' +
                ", condition='" + condition + '\'' +
                ", shareUrl='" + shareUrl + '\'' +
                '}';
    }
}
