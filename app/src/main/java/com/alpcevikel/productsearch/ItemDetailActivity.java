package com.alpcevikel.productsearch;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alpcevikel.productsearch.classes.CardData;
import com.alpcevikel.productsearch.classes.CombinedData;
import com.alpcevikel.productsearch.fragments.ItemPhotosFragment;
import com.alpcevikel.productsearch.fragments.ItemProductFragment;
import com.alpcevikel.productsearch.fragments.ItemShippingFragment;
import com.alpcevikel.productsearch.fragments.ItemSimilarFragment;
import com.alpcevikel.productsearch.utils.FacebookUtils;
import com.alpcevikel.productsearch.utils.StringUtils;
import com.alpcevikel.productsearch.utils.ToastUtils;

public class ItemDetailActivity extends AppCompatActivity {


    private static final String TAG = "ItemDetailActivity";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private CardData cardData;
    private Boolean wishStatus;
    private FloatingActionButton fab;
    private Context mContext;
    private LocalStorageManager localStorageManager;
    private int[] navIcons = {
            R.drawable.information_variant,
            R.drawable.truck_delivery,
            R.drawable.google,
            R.drawable.equal
    };
    private int[] navLabels = {
            R.string.tab_text_1,
            R.string.tab_text_2,
            R.string.tab_text_3,
            R.string.tab_text_4
    };
    // another resouces array for active state for the icon
    private int[] navIconsActive = {
            R.drawable.information_variant_active,
            R.drawable.truck_delivery_active,
            R.drawable.google_active,
            R.drawable.equal_active
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        setTabLayout(tabLayout);


        Log.d(TAG, "onCreate: Tab count is "+tabLayout.getTabCount());

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        setTabSelectedListener(tabLayout,mViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wishStatus) {
                    fab.setImageResource(R.drawable.cart_plus_fab);
                    ToastUtils.itemRemoveToast(mContext,cardData.title);
                    localStorageManager.removeObject(cardData.itemId);
                    wishStatus = false;
                } else {
                    fab.setImageResource(R.drawable.cart_remove_fab);
                    ToastUtils.itemAddToast(mContext,cardData.title);
                    localStorageManager.putObject(cardData);
                    wishStatus = true;
                }
            }
        });

        Intent intent = getIntent();
        CombinedData tx = (CombinedData) intent.getSerializableExtra("test");
        mContext = this;
        localStorageManager = new LocalStorageManager(this);
        cardData = tx.getCardData();
        wishStatus = tx.getaBoolean();
        if (wishStatus) {
            fab.setImageResource(R.drawable.cart_remove_fab);
        } else {
            fab.setImageResource(R.drawable.cart_plus_fab);
        }
        toolbar.setTitle(StringUtils.cropString(cardData.title,35));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_detail, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            String tx = FacebookUtils.getUri(cardData.shareUrl,cardData.title,cardData.price);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(tx));
            startActivity(i);
            return true;
        } else {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position==0) {
                return ItemProductFragment.newInstance(cardData.itemId,cardData.shippingCost);
            } else if (position==1) {
                return ItemShippingFragment.newInstance(cardData.itemId,cardData.shippingCost);
            } else if (position==2) {
                return ItemPhotosFragment.newInstance(cardData.title);
            } else {
                return ItemSimilarFragment.newInstance(cardData.itemId);
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

    }
    public void setTabLayout(TabLayout tabLayout) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            // inflate the Parent LinearLayout Container for the tab
            // from the layout nav_tab.xml file that we created 'R.layout.nav_tab
            LinearLayout tab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);
            // get child TextView and ImageView from this layout for the icon and label
            TextView tab_label = (TextView) tab.findViewById(R.id.nav_label);
            ImageView tab_icon = (ImageView) tab.findViewById(R.id.nav_icon);

            // set the label text by getting the actual string value by its id
            // by getting the actual resource value `getResources().getString(string_id)`
            tab_label.setText(getResources().getString(navLabels[i]));

            // set the home to be active at first
            if(i == 0) {
                tab_label.setTextColor(getColor(R.color.color_text_wh)
                );
                tab_icon.setImageResource(navIconsActive[i]);
            } else {
                tab_label.setTextColor(getColor(R.color.color_text_gray));
                tab_icon.setImageResource(navIcons[i]);
            }

            // finally publish this custom view to navigation tab
            tabLayout.getTabAt(i).setCustomView(tab);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown: Keydown here");
        return super.onKeyDown(keyCode, event);
    }

    public void setTabSelectedListener(TabLayout navigation, ViewPager viewPager) {
        navigation.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                View tabView = tab.getCustomView();
                TextView tab_label= (TextView) tabView.findViewById(R.id.nav_label);
                ImageView tab_icon = (ImageView) tabView.findViewById(R.id.nav_icon);
                tab_label.setTextColor(getColor(R.color.color_text_wh));
                tab_icon.setImageResource(navIconsActive[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                View tabView = tab.getCustomView();
                TextView tab_label= (TextView) tabView.findViewById(R.id.nav_label);
                ImageView tab_icon = (ImageView) tabView.findViewById(R.id.nav_icon);
                tab_label.setTextColor(getColor(R.color.color_text_gray));
                tab_icon.setImageResource(navIcons[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });




    }
}
