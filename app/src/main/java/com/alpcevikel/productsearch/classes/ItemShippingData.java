package com.alpcevikel.productsearch.classes;

import android.content.Intent;
import android.util.Log;

import org.json.JSONObject;

public class ItemShippingData {
    private static final String TAG = "ItemShippingData";

    public final String ITEM_NOT_FOUND = ItemProductData.ITEM_NOT_EXIST;
    public String storeName;
    public String storeUrl;
    public String feedbackScore;
    public String feedbackPercent;
    public String shippingCost;
    public String globalShipping;
    public String handlingTime;
    public String condition;
    public String returnPolicy;
    public String returnsWithin;
    public String refundMode;
    public String shippedBy;
    public String shippingText; //TODO fix this
    public String handlingText;
    
    private JSONObject itemObject; 

    public  ItemShippingData(JSONObject jsonObject,String shippingCost) {
        this.shippingCost = shippingCost;
        if (shippingCost.equals("N/A") || shippingCost.equals(ITEM_NOT_FOUND)) {
            this.shippingText = "N/A";
        } else if (shippingCost.equals("0.0")) {
            this.shippingText = "Free Shipping";
        } else {
            this.shippingText = "$"+shippingCost;
        }

        Log.d(TAG, "ItemShippingData: data recvd");
        try {
            itemObject = jsonObject.getJSONObject("Item");
            setStoreName();
            setStoreUrl();
            setFeedbackScore();
            setFeedbackPercent();
            setGlobalShipping();
            setHandlingTime();
            setCondition();
            setReturnPolicy();
            setReturnsWithin();
            setRefundMode();
            setShippedBy();
        } catch (Exception e ) {
            Log.d(TAG, "ItemShippingData: Couldnt find item key ");
        }
    }

    public void setStoreName() {
        try {
            this.storeName = itemObject.getJSONObject("Storefront").getString("StoreName");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch store name");
            this.storeName = this.ITEM_NOT_FOUND;
        }
    }

    public void setStoreUrl() {
        try {
            this.storeUrl = itemObject.getJSONObject("Storefront").getString("StoreURL");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch store url");
            this.storeUrl = this.ITEM_NOT_FOUND;
        }
    }

    public void setFeedbackScore() {
        try {
            int fs = itemObject.getJSONObject("Seller").getInt("FeedbackScore");
            this.feedbackScore = String.valueOf(fs);
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch feedback score");
            this.feedbackScore = this.ITEM_NOT_FOUND;
        }
    }

    public void setFeedbackPercent() {
        try {
            double fp = itemObject.getJSONObject("Seller").getDouble("PositiveFeedbackPercent");
            this.feedbackPercent = String.valueOf(fp);
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch feedback percent");
            this.feedbackPercent = this.ITEM_NOT_FOUND;
        }
    }


    public void setGlobalShipping( ) {
        try {
            if (itemObject.getBoolean("GlobalShipping")) {
             this.globalShipping = "Yes";
            } else {
                this.globalShipping = "No";
            }
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch global shipping");
            this.globalShipping = this.ITEM_NOT_FOUND;
        }
    }

    public void setHandlingTime() {
        try {
            this.handlingTime = String.valueOf(itemObject.getInt("HandlingTime"));
            if (itemObject.getInt("HandlingTime")<=1) {
                this.handlingText = this.handlingTime+" day";
            } else {
                this.handlingText = this.handlingTime+" days";
            }
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch handling time");
            this.handlingTime = this.ITEM_NOT_FOUND;
        }
    }

    public void setCondition() {
        try {
            this.condition = itemObject.getString("ConditionDisplayName");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch condition");
            this.condition = this.ITEM_NOT_FOUND;
        }
    }

    public void setReturnPolicy( ) {
        try {
            this.returnPolicy = itemObject.getJSONObject("ReturnPolicy").getString("ReturnsAccepted");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch retunr policy");
            this.returnPolicy = this.ITEM_NOT_FOUND;
        }
    }

    public void setReturnsWithin( ) {
        try {
            this.returnsWithin = itemObject.getJSONObject("ReturnPolicy").getString("ReturnsWithin");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch returns within");
            this.returnsWithin = this.ITEM_NOT_FOUND;
        }
    }

    public void setRefundMode( ) {
        try {
            this.refundMode = itemObject.getJSONObject("ReturnPolicy").getString("Refund");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch returns within");
            this.refundMode = this.ITEM_NOT_FOUND;
        }
    }

    public void setShippedBy( ) {
        try {
            this.shippedBy = itemObject.getJSONObject("ReturnPolicy").getString("ShippingCostPaidBy");
        } catch (Exception e ) {
            Log.d(TAG, "setStoreName: Failed to fetch returns shippedby");
            this.shippedBy = this.ITEM_NOT_FOUND;
        }
    }

    @Override
    public String toString() {
        return "ItemShippingData{" +
                "storeName='" + storeName + '\'' +
                ", storeUrl='" + storeUrl + '\'' +
                ", feedbackScore=" + feedbackScore +
                ", feedbackPercent=" + feedbackPercent +
                ", shippingCost='" + shippingCost + '\'' +
                ", globalShipping='" + globalShipping + '\'' +
                ", handlingTime='" + handlingTime + '\'' +
                ", condition='" + condition + '\'' +
                ", returnPolicy='" + returnPolicy + '\'' +
                ", returnsWithin='" + returnsWithin + '\'' +
                ", refundMode='" + refundMode + '\'' +
                ", shippedBy='" + shippedBy + '\'' +
                '}';
    }
}
