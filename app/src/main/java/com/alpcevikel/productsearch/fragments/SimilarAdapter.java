package com.alpcevikel.productsearch.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.SimilarRecyclerListener;
import com.alpcevikel.productsearch.classes.SimilarCardData;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SimilarAdapter extends RecyclerView.Adapter<SimilarAdapter.MyViewHolder> {
    private static final String TAG = "SimilarAdapter";
    private ArrayList<SimilarCardData> mDataset;
    private Context parentContext;
    private SimilarRecyclerListener itemListener;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public ImageView mimageView;
        public TextView  mTitleTextView;
        public TextView  mShippingTextView;
        public TextView mDaysLeftTextView;
        public TextView mPriceTextView;
        public MyViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            mimageView = (ImageView) v.findViewById(R.id.similarImage);
            mTitleTextView = (TextView) v.findViewById(R.id.similarTitle);
            mShippingTextView = (TextView) v.findViewById(R.id.similarShipping);
            mDaysLeftTextView = (TextView) v.findViewById(R.id.similarDaysLeft);
            mPriceTextView = (TextView) v.findViewById(R.id.similarPrice);
        }

        @Override
        public void onClick(View v) {
            itemListener.listItemClicked(v,getLayoutPosition());
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SimilarAdapter(ArrayList<SimilarCardData> myDataset, SimilarRecyclerListener itemListener) {
        this.itemListener = itemListener;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SimilarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        Log.d(TAG, "onCreateViewHolder: Starting create view holder");
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.similar_card_layout, parent, false);
        parentContext = parent.getContext();
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        SimilarCardData curData = mDataset.get(position);
        //holder.mimageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(parentContext).load(curData.imageUrl).into(holder.mimageView);
        holder.mTitleTextView.setText(curData.title);
        if (curData.shippingCost==0) {
            holder.mShippingTextView.setText("Free Shipping");
        } else {
            holder.mShippingTextView.setText("$"+String.valueOf(curData.shippingCost));
        }
        holder.mDaysLeftTextView.setText(String.valueOf(curData.daysLeft)+" Days Left");
        holder.mPriceTextView.setText("$"+String.valueOf(curData.price));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}