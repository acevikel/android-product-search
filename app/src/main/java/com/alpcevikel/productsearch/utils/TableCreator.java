package com.alpcevikel.productsearch.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.alpcevikel.productsearch.R;
import com.alpcevikel.productsearch.classes.ItemProductData;
import com.alpcevikel.productsearch.classes.ItemShippingData;
import com.wssholmes.stark.circular_score.CircularScoreView;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;


//TODO update params as static member var
//TODO set itemless rows
public class TableCreator {
    private static final int TABLE_MARGIN = 50;
    private static final int ITEM_MARGIN = 90;
    private static final int HEADER_SPACE = 20;
    private static final int ROW_SPACE  = 50;
    private static final int TABLE_ITEM_SPACE = 80;
    private static final int HEADER_SIZE = 20;
    private static final int DRAWABLE_PADDING = 20;
    private TableLayout tableLayout;
    private TextView header;
    private Context mContext;
    private ArrayList<TableRow> rows;
    private static final String TAG = "TableCreator";

    public  TableCreator(Context context) {
        mContext = context;
        tableLayout = new TableLayout(context);
        LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(-2,-2);
        par.setMarginStart(TABLE_MARGIN);
        tableLayout.setLayoutParams(par);

        rows = new ArrayList<>();
    }

    public void setTextHeader(String title, int draw) {
        header = new TextView(mContext);
        header.setText(title);
        header.setTextSize(HEADER_SIZE);
        header.setCompoundDrawablesWithIntrinsicBounds(draw,0,0,0);
        header.setCompoundDrawablePadding(DRAWABLE_PADDING);
        header.setTypeface(null,Typeface.BOLD);
        TableLayout.LayoutParams par = new TableLayout.LayoutParams(-2,-2);
        par.setMargins(0,50 ,HEADER_SPACE,0);
        header.setLayoutParams(par);

    }

    public void addKeyValueRow(String key, String value) {
        //TODO newline if long
        if (value==null) return;
        if (value.equals(ItemProductData.ITEM_NOT_EXIST) || value.equals("N/A")) return;
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(400, -2);
        layoutParams.setMarginStart(ITEM_MARGIN);
        layoutParams.weight = 1;
        layoutParams.column = 1;
        TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(700, -2);
        layoutParams2.setMarginStart(TABLE_ITEM_SPACE);
        layoutParams2.weight = 2;
        layoutParams2.column=2;

        TableRow tr = new TableRow(mContext);
        TableLayout.LayoutParams rowparams = new TableLayout.LayoutParams(-2, -2);
        rowparams.setMargins(0,ROW_SPACE,0,0);
        tr.setLayoutParams(rowparams);

        TextView tv  = new TextView(mContext);
        tv.setLayoutParams(layoutParams);
        tv.setTypeface(null,Typeface.BOLD);
        tv.setText(key+": ");
        tr.addView(tv);

        TextView tv2  = new TextView(mContext);
        tv2.setLayoutParams(layoutParams2);
        tv2.setText(value);
        tv2.setSingleLine(false);
        //TODO FIX MULTI LINE
        tr.addView(tv2);

        rows.add(tr);
    }


    public void addStarRow(String key, String value) {
        //TODO newline if long
        if (value.equals(ItemProductData.ITEM_NOT_EXIST)) return;
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-2, -2);
        layoutParams.setMarginStart(ITEM_MARGIN);
        layoutParams.column = 1;
        TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(-2, -2);
        layoutParams2.gravity = Gravity.LEFT;
        layoutParams2.setMarginStart(0);
        layoutParams2.column=2;

        TableRow tr = new TableRow(mContext);
        TableLayout.LayoutParams rowparams = new TableLayout.LayoutParams(-2, -2);
        rowparams.setMargins(0,ROW_SPACE,0,0);
        tr.setLayoutParams(rowparams);

        TextView tv  = new TextView(mContext);
        tv.setLayoutParams(layoutParams);
        tv.setTypeface(null,Typeface.BOLD);
        tv.setText(key+": ");
        tr.addView(tv);

        ImageView tv2  = new ImageView(mContext);
        tv2.setLayoutParams(layoutParams2);
        tv2.setImageDrawable(StarUtils.getStar(mContext,value));
        tr.addView(tv2);

        rows.add(tr);
    }

    public void addLinkedText(String key, String value, String link) {
        //TODO newline if long
        if (value.equals(ItemProductData.ITEM_NOT_EXIST)) return;
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-2, -2);
        layoutParams.setMarginStart(ITEM_MARGIN);
        layoutParams.column = 1;
        TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(-2, -2);
        layoutParams2.setMarginStart(0);
        layoutParams2.column=2;

        TableRow tr = new TableRow(mContext);
        TableLayout.LayoutParams rowparams = new TableLayout.LayoutParams(-2, -2);
        rowparams.setMargins(0,ROW_SPACE,0,0);
        tr.setLayoutParams(rowparams);

        TextView tv  = new TextView(mContext);

        tv.setLayoutParams(layoutParams);
        tv.setTypeface(null,Typeface.BOLD);
        tv.setText(key+": ");
        tr.addView(tv);

        TextView tv2  = new TextView(mContext);
        tv2.setLayoutParams(layoutParams2);
        tr.addView(tv2);
        tv2.setText(
                Html.fromHtml(
                        "<a href=\""+link+"\">"+value+"</a> "));
        tv2.setMovementMethod(LinkMovementMethod.getInstance());
        rows.add(tr);
    }



    public void addCircularScore(String key, String value, View view) {
        //TODO newline if long
        if (value.equals(ItemProductData.ITEM_NOT_EXIST)) return;
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-2, -2);
        layoutParams.setMarginStart(ITEM_MARGIN);
        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        layoutParams.column = 1;
        TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(75, 75);
        layoutParams2.setMarginStart(0);
        layoutParams2.column=2;

        TableRow tr = new TableRow(mContext);
        TableLayout.LayoutParams rowparams = new TableLayout.LayoutParams(-2, -2);
        rowparams.setMargins(0,ROW_SPACE,0,0);
        tr.setLayoutParams(rowparams);

        TextView tv  = new TextView(mContext);
        tv.setLayoutParams(layoutParams);
        tv.setTypeface(null,Typeface.BOLD);
        tv.setText(key+": ");
        tr.addView(tv);

        XmlPullParser parser = view.getResources().getXml(R.xml.test);
        try {
            parser.next();
            parser.nextTag();

        } catch (Exception e) {
            e.printStackTrace();
        }

        AttributeSet attr = Xml.asAttributeSet(parser);
        Log.d(TAG, "addCircularScore: Count is "+attr.getAttributeCount());
        CircularScoreView tv2  = new CircularScoreView(mContext,attr);
        tv2.setLayoutParams(layoutParams2);
        tv2.setScore(Math.round(Float.parseFloat(value)));
        tr.addView(tv2);
        rows.add(tr);
    }

    public void addBulletedRow(String desc) {
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-2, -2);
        layoutParams.setMarginStart(ITEM_MARGIN);
        layoutParams.setMargins(0,20,0,0);
        TableRow tr = new TableRow(mContext);
        TextView tv  = new TextView(mContext);
        tv.setText("\u2022 "+desc);
        tv.setLayoutParams(layoutParams);
        tr.addView(tv);
        rows.add(tr);
    }

    public TableLayout getTableLayout () {
        if (rows.size()==0) tableLayout.setVisibility(View.GONE);
        if (rows.size()>0) {
            tableLayout.addView(header);
        }
        for (TableRow r : rows) {
            tableLayout.addView(r);
        }
        return  tableLayout;
    }

    public  int getNumRows() {
        return this.rows.size();
    }

    public static View createDivider(Context context) {
        //TODO maybe change to Space
        View sp = new View(context);
        //Space sp = new Space(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,2);
        lp.setMargins(TABLE_MARGIN,ROW_SPACE,0,0);
        //sp.setPadding(TABLE_MARGIN+ITEM_MARGIN,0,0,0);
        sp.setBackgroundColor(context.getColor(R.color.color_transparent_gray));
        sp.setLayoutParams(lp);
        return sp;
    }




}
