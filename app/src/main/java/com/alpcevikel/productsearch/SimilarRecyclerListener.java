package com.alpcevikel.productsearch;

import android.view.View;

public interface SimilarRecyclerListener {
    public void listItemClicked(View v, int position);
}
