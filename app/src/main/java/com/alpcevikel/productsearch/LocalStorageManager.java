package com.alpcevikel.productsearch;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;

import com.alpcevikel.productsearch.classes.CardData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class LocalStorageManager {

    private static final String TAG = "LocalStorageManager";
    private  SharedPreferences sharedPreferences;
    private  Gson gson;

    public  LocalStorageManager(Context context) {
        sharedPreferences = context.getSharedPreferences("MyPreferences",context.MODE_PRIVATE);
        gson = new Gson();
    }

    public void putObject(CardData cd ){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Log.d(TAG, "putObject: Item id is "+cd.itemId);
        editor.putString(cd.itemId,gson.toJson(cd));
        editor.commit();
    }

    public CardData getObject(String itemId) {
        String json = sharedPreferences.getString(itemId,"");
        return gson.fromJson(json,CardData.class);
    }

    public void removeObject(String itemId){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(itemId);
        editor.commit();
    }

    public boolean hasItem(String itemId) {
        return sharedPreferences.contains(itemId);
    }

    public void clearAll() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public ArrayList<Pair<CardData,Boolean>> getDataset() {
        ArrayList<Pair<CardData,Boolean>> dataset = new ArrayList<>();
        Map<String,String> rm = (Map<String, String>) sharedPreferences.getAll();
        Set<String> objectSet = rm.keySet();
        for (String json: objectSet) {
            try {
                Pair<CardData, Boolean> pm = new Pair<>(gson.fromJson(rm.get(json), CardData.class), Boolean.TRUE);
                dataset.add(pm);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dataset;
    }
}